#!/usr/bin/env python
# -*- coding: utf-8 -*-
#映像に2値化を行って保存する

import cv2.cv as cv #前ver.のモジュール
import cv2
import sys
import rospy

windowName_1 = '入力画像'
windowName_2 = '出力画像'

cv2.namedWindow(windowName_1)
cv2.namedWindow(windowName_2)

src = cv2.VideoCapture('lalaland.avi')

if not src.isOpened():
    print '映像が取得できません'
    sys.exit()

retval, frame = src.read() #1フレーム取得
height, width, channels = frame.shape

rec = cv2.VideoWriter('Movie_out.avi', cv.CV_FOURCC('X','V','I','D'), 30, (width, height))
#ビデオライタrecの取得
#cv2.videoWriter(保存ファイル名，ビデオコーデックの種類，フレームレート，画像サイズ，（カラー(3ch):True or グレー(1ch):False）)

while True:
    retval, frame = src.read()
    if frame is None:
        break

    img_out = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) #グレースケール変換
    retval, img_out = cv2.threshold(img_out, 64, 255, cv2.THRESH_BINARY) #2値化処理
    img_out = cv2.cvtColor(img_out, cv2.COLOR_GRAY2BGR)

    cv2.imshow(windowName_1, frame)
    cv2.imshow(windowName_2, img_out)

    rec.write(img_out)

    key = cv2.waitKey(33)
    if key == 27:
        break

cv2.destroyAllWindows()
src.release()
rec.release()