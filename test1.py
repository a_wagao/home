#!/usr/bin/env python
# -*- coding: utf-8 -*-
#カメラ映像表示→映像表示されない

import cv2

windowName = "カメラ映像"
cv2.namedWindow(windowName)

src = cv2.VideoCapture(0)
#引数に0→内蔵カメラorUSBカメラ(両方あるときは-1)


if not src.isOpened():
    print 'カメラ映像が取得できません'
    import sys
    sys.exit()

while True:
    retval, frame = src.read()
   # cv2.imshow(windowName, frame)

   #これいらんきがする
    if frame is None: #映像終了時
        break

        cv2.imshow(windowName.frame) #1フレーム表示

        key = cv2.waitKey(33)
        if key == 27:
            break

cv2.destroyAllWindows()
src.release()